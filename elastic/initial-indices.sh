#!/bin/sh
#set -eux

host="localhost:9200"

waitforurl() {
    echo "Testing $1"
    timeout -s TERM 45 bash -c \
    'while [[ "$(curl -s -o /dev/null -L -w ''%{http_code}'' ${0})" != "200" ]];\
    do echo "Waiting for ${0}" && sleep 3;\
    done' ${1}
    echo "OK!"
    curl -I $1
}

waitforurl $host

>&2 echo "Elastic is up - resetting index specifications"
curl -X DELETE "$host/problem_sets"
curl -X PUT "$host/problem_sets" -H 'Content-Type: application/json' --data "@indices/problem_sets.elastic_mapping.json"
curl -X DELETE "$host/organizations"
curl -X PUT "$host/organizations" -H 'Content-Type: application/json' --data "@indices/organizations.elastic_mapping.json"
curl -X DELETE "$host/taxonomies"
curl -X PUT "$host/taxonomies" -H 'Content-Type: application/json' --data "@indices/taxonomies.elastic_mapping.json"


>&2 echo "Index specifications pushed, bringing up Neo"
exec $cmd
