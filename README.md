[![pipeline status](https://gitlab.com/inqtel/arq-api/badges/production/pipeline.svg)](https://gitlab.com/inqtel/arq-api/-/commits/production)
[![coverage report](https://gitlab.com/inqtel/arq-api/badges/production/coverage.svg)](https://gitlab.com/inqtel/arq-api/-/commits/production)

# ArQ API

`arq-api` is the backend for the [ArQ](https://gitlab.com/inqtel/arq) project. It provides:
 * GraphQL API over Postgres 
 * Logstash service to sync PostgreSQL data to ElasticSearch
 * Cache Layer via ElasticSearch
 * Optional Gitlab CI job to sync your SalesForce objects to PostgreSQL


<img src="https://gitlab.com/inqtel/arq-api/uploads/4b3d4629f92bbc25efbf26b656f0f522/arq-api.png" width="500" alt="ArQ Diagram">


## Developer Quickstart (Mac OSX)

### Requirements
- [Install Docker](https://docs.docker.com/docker-for-mac/install/)
- [Add SSH Key to Github](https://help.github.com/en/github/authenticating-to-github/adding-a-new-ssh-key-to-your-github-account)
- [Install Hasura CLI](https://hasura.io/docs/1.0/graphql/core/hasura-cli/install-hasura-cli.html)

### Clone
Clone this project.

```console
[development root]#> cd arq-api
```

### Credentials

Copy and paste the following into .env file and save.

```
ARQ_ADMINS=mcurie@example.com,efranklin@example.com
ARQ_CORSORIGINWHITELIST=http://localhost:8005
ARQ_ELEVATED_USER_TYPE=employee
ES_HOST=http://elastic:9200
HASURA_GRAPHQL_ADMIN_SECRET=myadminsecretkey
POSTGRES_USER=testuser
POSTGRES_PASSWORD=testpass
POSTGRES_DB=arq
DATABASE_HOST=postgres
```


### Run Startup Script 

```console
./startup.sh
```

## Development Tools

### Hasura GraphQL Engine

```console
[project root]#> cd hasura
[project root]#> hasura console
```

A browser opens automatically to [http://localhost:9695/](http://localhost:9695/)


### ElasticSearch

To see ElasticSearch running go to: [http://localhost:9200](http://localhost:9200)

To get high-level information about your indices go to: [http://localhost:9200/_cat/indices](http://localhost:9200/_cat/indices)

To interact with [Dejavu (web UI for Elasticsearch)](https://opensource.appbase.io/dejavu/) go to: [http://localhost:1358](http://localhost:1358)


### Interact with API

```console
# View containers
#> docker ps

# Run bash inside containers
#> docker exec -it $container_name /bin/bash
# Example:
#> docker exec -it arq-api_hasura_1 /bin/bash

# Tail Logs
#> docker logs -f $(docker ps | grep -w $container_name | awk '{print $1}')
# Example:
#> docker logs -f $(docker ps | grep -w arq-api_postgres_1 | awk '{print $1}')
```


### Unit Testing

**arq-api** runs its [pgTAP](https://pgtap.org/documentation.html) unit tests against an image built and stored in [postgres12-pgtap's](https://gitlab.com/inqtel/postgres12-pgtap#postgres12-pgtap) Container Registry. [Tests can be run locally](https://gitlab.com/inqtel/arq-api/-/blob/production/postgres/pgtap/README.md). Tests will also run on every push to the repository. (See [pg-unit-tests](https://gitlab.com/inqtel/arq-api/-/blob/production/.gitlab-ci.yml) GitLab CI job)

All tests must pass and coverage should stay above <strong>10%</strong> before code can be merged in.


### Safesforce Sync (Optional)
Update `.env` in your editor of choice and set the SalesForce values pertaining to your SalesForce instance.

The Gitlab CI `salesforce-sync` manual job syncs your Salesforce Objects to Postgres. You can also add this job as a cron job in your production environment.

```
ARQ_SALESFORCEURL=
ARQ_SALESFORCEUSER=
ARQ_SALESFORCEPASSWORD=
ARQ_SALESFORCETOKEN=
```

## Authentication
ArQ's instance of Hasura is configured to run with an admin secret and rejects any unauthenticated requests it receives. Hasura requires the following request headers to be set in each request:

```
X-Hasura-Admin-Secret: $HASURA_GRAPHQL_ADMIN_SECRET
X-Hasura-Remote-User: user_email
X-Hasura-Role: api-admin/api-user
X-Hasura-User-Type: user-type
```

Together, with the above implementation, a proxy service should handle authentication and the above HTTP header injection before proxying back to Hasura or Elastic Search. If you don't have a proxy or middleware, you will have to build your own [Authentication service](https://hasura.io/docs/1.0/graphql/core/auth/index.html).

Check out [arq-auth](https://gitlab.com/inqtel/arq-auth) for an authentication example using webhooks.


<img src="https://gitlab.com/inqtel/arq-api/uploads/897ec0aaadcbfa8dcd81ba2effe2e3e9/infra.png" alt="ArQ Infra Diagram">


## ENV Variables

For local development, env vars are set via `docker-compose.yml`. You should also copy the .env.example to .env in the root directory, and request the credentials from another developer

For staging and production, the following environment variables are set via the deploy process:

`ARQ_CORSORIGINWHITELIST` - this is a comma separated list of acceptable request Origins

`PG_CONNECTION_STR` - this is the database connection string

`ARQ_PORT` - the http port the application runs on


## Deployment

#### Development

Develop against the `development` branch in gitlab.  Do PR's against this branch. Squash commits when merging to the development branch.


#### Staging

To trigger a staging build, create a `release/{xx.xx.xx}-sth` branch from the latest `development` branch and push that branch. This will trigger a deployment to the staging servers.

#### Production

To deploy to production, merge the release branch into the `production` branch.  This will deploy to production servers.



## Health Checks

Health checks are located at `/_service/status` and displays the commit hash fo the release. Be sure to compare the hash against the expected branch hash when deploying.

## Contributing

Thanks for your interest in contributing to ArQ! Get started [here](https://gitlab.com/inqtel/arq-api/-/blob/master/CONTRIBUTING.md).

## Bidirectional Syncing

Contributing your code to ArQ's open source `production` branches will make its way to our internal `production` branches, and vice versa, for those contributing from the inside. Learn more about our bidirectional syncing process [here](https://gitlab.com/inqtel/arq-api/-/blob/production/BIDIRECTIONAL-SYNCING.md). 