CREATE FUNCTION arq.taxonomies_lower(taxonomies arq.taxonomies) RETURNS text AS
$$
SELECT LOWER(taxonomies.term)
$$ LANGUAGE sql STABLE;

CREATE FUNCTION arq.problem_sets_description(problem_set arq.problem_sets, hasura_session json)
    RETURNS text AS
$$
SELECT CASE
           WHEN hasura_session ->> 'x-hasura-user-type' =
                (SELECT value FROM arq.constants WHERE key = 'elevated-user-type') THEN ps.description
           END AS description
FROM arq.problem_sets ps
WHERE ps.id = problem_set.id;
$$ LANGUAGE sql STABLE;

CREATE FUNCTION arq.taxonomies_ancestors(taxonomy arq.taxonomies)
    RETURNS SETOF arq.taxonomies
AS
$$
BEGIN
    RETURN QUERY
        SELECT *
        FROM arq.taxonomies t
        WHERE t.id IN (WITH RECURSIVE ancestors(id, parent_id) AS (
            /* non-recursive section */
            SELECT a.id, a.parent_id
            FROM arq.taxonomies a
            WHERE a.id = taxonomy.id
            UNION ALL
            /* recursive section */
            SELECT parent.id, parent.parent_id
            FROM arq.taxonomies parent
                     JOIN ancestors children ON children.parent_id = parent.id)
                       SELECT id
                       FROM ancestors);
END
$$
    LANGUAGE plpgsql STABLE
                     STRICT;

CREATE FUNCTION arq.taxonomies_all_children(taxonomy arq.taxonomies)
    RETURNS SETOF arq.taxonomies
AS
$$
BEGIN
    RETURN QUERY
        SELECT *
        FROM arq.taxonomies t
        WHERE t.id IN (WITH RECURSIVE children(id, parent_id) AS (
            /* non-recursive section */
            SELECT id, parent_id
            FROM arq.taxonomies
            WHERE id = taxonomy.id
            UNION ALL

            /* recursive section */
            SELECT child.id,
                   child.parent_id
            FROM arq.taxonomies child
                     JOIN children parent ON parent.id = child.parent_id
        )
                       SELECT id
                       FROM children);
END
$$
    LANGUAGE plpgsql STABLE
                     STRICT;

CREATE FUNCTION arq.organizations_capabilities(organization arq.organizations)
    RETURNS SETOF arq.taxonomies
    LANGUAGE sql
    STABLE
AS
$function$
SELECT tax.*
FROM arq.taxonomies tax
         JOIN arq.organization_taxonomies org_tax
              ON tax.id = org_tax.taxonomy_id
WHERE org_tax.organization_id = organization.id
$function$;

CREATE FUNCTION arq.taxonomies_companies(taxonomy arq.taxonomies)
    RETURNS SETOF arq.organizations
    LANGUAGE sql
    STABLE
AS
$function$
SELECT org.*
FROM arq.organizations org
         JOIN arq.organization_taxonomies org_tax
              ON org.id = org_tax.organization_id
WHERE org_tax.taxonomy_id = taxonomy.id
$function$;

CREATE FUNCTION arq.taxonomies_problem_sets(taxonomy arq.taxonomies)
    RETURNS SETOF arq.problem_sets
    LANGUAGE sql
    STABLE
AS
$function$
SELECT ps.*
FROM arq.problem_sets ps
         JOIN arq.problem_set_taxonomies ps_tax
              ON ps.id = ps_tax.problem_set_id
WHERE ps_tax.taxonomy_id = taxonomy.id
$function$;

CREATE FUNCTION arq.problem_sets_capabilities(problem_set arq.problem_sets)
    RETURNS SETOF arq.taxonomies
    LANGUAGE sql
    STABLE
AS
$function$
SELECT tax.*
FROM arq.taxonomies tax
         JOIN arq.problem_set_taxonomies ps_tax
              ON tax.id = ps_tax.taxonomy_id
WHERE ps_tax.problem_set_id = problem_set.id
$function$;

CREATE FUNCTION arq.customers_capabilities(customer arq.customers)
    RETURNS SETOF arq.taxonomies
    LANGUAGE sql
    STABLE
AS
$function$
SELECT DISTINCT(arq.problem_sets_capabilities(ps))
FROM arq.problem_sets ps
WHERE ps.customer_id = customer.id
$function$;

CREATE FUNCTION arq.customers_architectures(customer arq.customers)
    RETURNS SETOF arq.architectures
    LANGUAGE sql
    STABLE
AS
$function$
SELECT DISTINCT(arch.*)
FROM arq.architectures arch,
     arq.packages pack,
     arq.taxonomies tax
WHERE arch.published_at IS NOT NULL
  AND arch.next_version_id IS NULL
  AND pack.architecture_id = arch.id
  AND tax.id = pack.taxonomy_id
  AND tax.id IN (SELECT DISTINCT(arq.problem_sets_capabilities(ps)).id
                 FROM arq.problem_sets ps
                 WHERE ps.customer_id = customer.id);
$function$;

CREATE FUNCTION arq.customers_companies(customer arq.customers)
    RETURNS SETOF arq.organizations
    LANGUAGE sql
    STABLE
AS
$function$
SELECT DISTINCT(arq.taxonomies_companies(tax))
FROM arq.taxonomies tax,
     arq.problem_sets ps
WHERE tax.id IN (SELECT DISTINCT(arq.problem_sets_capabilities(ps)).id
                 FROM arq.problem_sets ps
                 WHERE ps.customer_id = customer.id);
$function$;

CREATE FUNCTION arq.latest_architecture(architecture arq.architectures)
    RETURNS SETOF arq.architectures AS
$$
BEGIN
    RETURN QUERY
        SELECT *
        FROM arq.architectures a
        WHERE a.id IN (WITH RECURSIVE latest_archs(id, next_version_id) AS (
            SELECT la.id, la.next_version_id
            FROM arq.architectures la
            WHERE la.id = architecture.id
            UNION ALL
            SELECT next_version.id, next_version.next_version_id
            FROM arq.architectures next_version
                     JOIN latest_archs nvs ON nvs.next_version_id = next_version.id)
                       SELECT id
                       FROM latest_archs
                       WHERE next_version_id IS NULL);
END
$$ LANGUAGE plpgsql STABLE;

CREATE FUNCTION arq.architectures_is_latest_arch(architecture arq.architectures)
    RETURNS boolean AS
$$
SELECT EXISTS(
               SELECT *
               FROM arq.architectures
               WHERE next_version_id IS NULL
                 AND id = architecture.id
           ) AS isLatestArch
$$ LANGUAGE sql STABLE;

CREATE FUNCTION arq.published_architectures()
    RETURNS SETOF arq.architectures
    LANGUAGE sql
    STABLE
AS
$function$
SELECT *
FROM arq.architectures
WHERE published_at IS NOT NULL
  AND next_version_id IS NULL
$function$;

CREATE FUNCTION arq.architectures_package_roots(architecture arq.architectures)
    RETURNS SETOF arq.packages
    LANGUAGE sql
    STABLE
AS
$function$
SELECT pkg.*
FROM arq.packages pkg
WHERE pkg.architecture_id = architecture.id
  AND pkg.parent_id IS NULL
$function$;

CREATE FUNCTION arq.architectures_status(architecture arq.architectures)
    RETURNS text
AS
$$
DECLARE
    latest timestamp;
    status text;
BEGIN
    latest = architecture.draft_at;
    status = 'draft';


    IF (architecture.submitted_at IS NOT NULL) AND (architecture.submitted_at > latest) THEN
        latest = architecture.submitted_at;
        status = 'submitted';
    END IF;

    IF (architecture.in_review_at IS NOT NULL) AND (architecture.in_review_at > latest) THEN
        latest = architecture.in_review_at;
        status = 'inreview';
    END IF;

    IF (architecture.rejected_at IS NOT NULL) AND (architecture.rejected_at > latest) THEN
        latest = architecture.rejected_at;
        status = 'rejected';
    END IF;

    IF (architecture.published_at IS NOT NULL) AND (architecture.published_at > latest) THEN
        latest = architecture.published_at;
        status = 'published';
    END IF;

    RETURN status;
END;
$$
    LANGUAGE plpgsql STABLE;

CREATE FUNCTION arq.collaboration_invitations_status(collaboration_invitation arq.collaboration_invitations) RETURNS text AS
$$
SELECT CASE
           WHEN seen_at IS NOT NULL THEN 'seen'
           ELSE 'unseen'
           END
FROM arq.collaboration_invitations
WHERE id = collaboration_invitation.id;
$$ LANGUAGE sql STABLE;

CREATE FUNCTION arq.taxonomy_sources_roots(taxonomy_source arq.taxonomy_sources)
    RETURNS SETOF arq.taxonomies
    LANGUAGE sql
    STABLE
AS
$function$
SELECT tax.*
FROM arq.taxonomies tax
WHERE tax.source_id = taxonomy_source.id
  AND tax.parent_id IS NULL
$function$;

CREATE FUNCTION arq.taxonomies_root(taxonomy arq.taxonomies)
    RETURNS SETOF arq.taxonomies
AS
$$
BEGIN
    RETURN QUERY
        SELECT *
        FROM arq.taxonomies t
        WHERE t.id IN (WITH RECURSIVE ancestors(id, parent_id) AS (
            /* non-recursive section */
            SELECT a.id, a.parent_id
            FROM arq.taxonomies a
            WHERE a.id = taxonomy.id
            UNION ALL
            /* recursive section */
            SELECT parent.id, parent.parent_id
            FROM arq.taxonomies parent
                     JOIN ancestors children ON children.parent_id = parent.id)
                       SELECT id
                       FROM ancestors
                       WHERE parent_id IS NULL);
END
$$
    LANGUAGE plpgsql STABLE
                     STRICT;

CREATE FUNCTION arq.referenced_taxonomy_roots(taxonomy_source arq.taxonomy_sources)
    RETURNS setof arq.taxonomies
AS
$BODY$
BEGIN
    RETURN QUERY SELECT t.*
                 FROM arq.taxonomies t
                          INNER JOIN arq.packages p ON p.taxonomy_id = t.id
                          INNER JOIN arq.architectures a ON p.architecture_id = a.id
                 WHERE source_id = taxonomy_source.id
                   AND arq.architectures_status(a) = 'published'
                   AND ((t.parent_id IS NULL)
                     OR NOT exists(SELECT id FROM arq.packages pp WHERE pp.taxonomy_id = t.parent_id));
END;
$BODY$
    LANGUAGE plpgsql STABLE
                     STRICT;

CREATE FUNCTION arq.packages_ancestors(package arq.packages)
    RETURNS SETOF arq.packages
AS
$$
BEGIN
    RETURN QUERY
        SELECT *
        FROM arq.packages t
        WHERE t.id IN (WITH RECURSIVE ancestors(id, parent_id) AS (
            /* non-recursive section */
            SELECT a.id, a.parent_id
            FROM arq.packages a
            WHERE a.id = package.id
            UNION ALL
            /* recursive section */
            SELECT parent.id, parent.parent_id
            FROM arq.packages parent
                     JOIN ancestors children ON children.parent_id = parent.id)
                       SELECT id
                       FROM ancestors);
END
$$
    LANGUAGE plpgsql STABLE
                     STRICT;

CREATE FUNCTION arq.architectures_statuses(statuses TEXT[])
    RETURNS SETOF arq.architectures AS
$$
SELECT *
FROM arq.architectures a
WHERE (arq.architectures_status(a) = ANY (statuses));
$$ LANGUAGE sql STABLE;

CREATE FUNCTION arq.get_session_role(hasura_session json)
    RETURNS SETOF arq.text_result AS
$$
SELECT q.*
FROM (VALUES (hasura_session ->> 'x-hasura-role')) q
$$ LANGUAGE sql STABLE;

CREATE FUNCTION arq.get_session_user(hasura_session json)
    RETURNS SETOF arq.text_result AS
$$
SELECT q.*
FROM (VALUES (hasura_session ->> 'x-hasura-remote-user')) q
$$ LANGUAGE sql STABLE;


-- VIEWS
CREATE VIEW arq.auth_user AS
SELECT *
FROM arq.users;

CREATE VIEW arq.my_documents AS
SELECT *
FROM arq.documents;

CREATE VIEW arq.my_collaboration_invitations AS
SELECT CASE
           WHEN seen_at IS NOT NULL THEN 'seen'
           ELSE 'unseen'
           END
           AS status,
       ci.*
FROM arq.collaboration_invitations ci;

CREATE VIEW arq.my_architectures AS
SELECT arq.architectures_status(arch)
           AS status,
       arch.*
FROM arq.architectures arch;

CREATE VIEW arq.document_blocks AS
SELECT doc.id AS document_id, b.body, b.architecture_id
FROM arq.documents doc,
     unnest(doc.document_blocks) AS b;

CREATE VIEW arq.integrations AS
SELECT current_setting('arq.salesforce_enabled') AS salesforce_enabled;

-- TRIGGERS
CREATE FUNCTION arq.set_current_timestamp_updated_at()
    RETURNS TRIGGER AS
$$
DECLARE
    _new record;
BEGIN
    _new := NEW;
    _new.updated_at = NOW();
    RETURN _new;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER set_arq_architectures_updated_at
    BEFORE UPDATE
    ON arq.architectures
    FOR EACH ROW
EXECUTE PROCEDURE arq.set_current_timestamp_updated_at();

CREATE TRIGGER set_arq_packages_updated_at
    BEFORE UPDATE
    ON arq.packages
    FOR EACH ROW
EXECUTE PROCEDURE arq.set_current_timestamp_updated_at();

COMMENT ON TRIGGER set_arq_packages_updated_at ON arq.packages
    IS 'trigger to set value of column "updated_at" to current timestamp on row update';

COMMENT ON TRIGGER set_arq_architectures_updated_at ON arq.architectures
    IS 'trigger to set value of column "updated_at" to current timestamp on row update';

CREATE TRIGGER set_arq_documents_updated_at
    BEFORE UPDATE
    ON arq.documents
    FOR EACH ROW
EXECUTE PROCEDURE arq.set_current_timestamp_updated_at();

COMMENT ON TRIGGER set_arq_documents_updated_at ON arq.documents
    IS 'trigger to set value of column "updated_at" to current timestamp on row update';

CREATE TRIGGER set_arq_taxonomies_updated_at
    BEFORE UPDATE
    ON arq.taxonomies
    FOR EACH ROW
EXECUTE PROCEDURE arq.set_current_timestamp_updated_at();

COMMENT ON TRIGGER set_arq_taxonomies_updated_at ON arq.taxonomies
    IS 'trigger to set value of column "updated_at" to current timestamp on row update';

CREATE TRIGGER set_arq_collaboration_invitations_updated_at
    BEFORE UPDATE
    ON arq.collaboration_invitations
    FOR EACH ROW
EXECUTE PROCEDURE arq.set_current_timestamp_updated_at();

COMMENT ON TRIGGER set_arq_collaboration_invitations_updated_at ON arq.collaboration_invitations
    IS 'trigger to set value of column "updated_at" to current timestamp on row update';

-- publish architecture

CREATE VIEW arq.publish_architectures_mutation_view AS
SELECT *
FROM arq.architectures;


CREATE FUNCTION arq.publish_insert_package(new_id uuid, new_parent_id uuid, new_architecture_id uuid,
                                           package arq.packages)
    RETURNS SETOF arq.packages
AS
$BODY$
BEGIN
    INSERT INTO arq.packages(id, architecture_id, parent_id, term, description, taxonomy_id,
                             reference_package_id, level_0_position, level_0_title_position, level_1_x,
                             level_1_y, level_1_width, level_1_height, level_1_drawn, level_1_hide_children,
                             level_1_columns, level_1_position, level_1_title_position, level_2_position, sequence)
    VALUES (new_id, new_architecture_id, new_parent_id, package.term, package.description,
            package.taxonomy_id, package.reference_package_id, package.level_0_position,
            package.level_0_title_position, package.level_1_x, package.level_1_y, package.level_1_width,
            package.level_1_height, package.level_1_drawn, package.level_1_hide_children,
            package.level_1_columns, package.level_1_position, package.level_1_title_position,
            package.level_2_position, package.sequence);

    RETURN QUERY SELECT * FROM arq.packages WHERE id = new_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.transfer_package_tree(arch_id uuid, prnt_id uuid, package arq.packages)
    RETURNS setof arq.packages
AS
$$
DECLARE
    new_id uuid;
    child  arq.packages%rowtype;
BEGIN
    new_id = public.uuid_generate_v4();

    PERFORM arq.publish_insert_package(new_id, prnt_id, arch_id, package);

    FOR child IN SELECT * FROM arq.packages WHERE parent_id = package.id
        LOOP
            PERFORM arq.transfer_package_tree(arch_id, new_id, child);
        END LOOP;

    RETURN QUERY SELECT * FROM arq.packages WHERE id = new_id;
END;
$$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.publish_architecture(architecture_id uuid, email varchar(255))
    RETURNS SETOF arq.architectures
AS
$$
DECLARE
    arch        arq.architectures%ROWTYPE;
    package     arq.packages%ROWTYPE;
    new_id      uuid;
    new_parent  uuid;
    latest_arch arq.architectures%ROWTYPE;
BEGIN
    -- retrieve arch or fail
    SELECT *
    INTO arch
    FROM arq.architectures
    WHERE id = architecture_id
      AND submitted_at IS NOT NULL
      AND in_review_at IS NOT NULL;
    IF NOT FOUND THEN
        RAISE EXCEPTION 'architecture % not found', architecture_id;
    END IF;

    -- clone relevant data into a new arch
    new_id = uuid_generate_v4();
    INSERT INTO arq.architectures (id, submitted_at, in_review_at, published_at, title, publisher_email,
                                   submitter_email, draft_at)
    VALUES (new_id, arch.submitted_at, arch.in_review_at, now(), arch.title, email, arch.submitter_email,
            arch.draft_at);

    -- set old arch back to draft
    UPDATE arq.architectures
    SET draft_at = now()
    WHERE id = arch.id;

    -- set latest version chain
    latest_arch = arq.latest_architecture(arch);
    UPDATE arq.architectures
    SET next_version_id = new_id
    WHERE id = latest_arch.id;


    -- iterate through packages and clone
    FOR package IN SELECT * FROM arq.packages p WHERE (p.parent_id IS NULL) AND (p.architecture_id = arch.id)
        LOOP
            PERFORM arq.transfer_package_tree(new_id, NULL, package);
        END LOOP;

    RETURN QUERY SELECT * FROM arq.architectures WHERE id = new_id;
END;
$$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION publish_architecture_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    new_arch arq.architectures;
BEGIN
    new_arch = arq.publish_architecture(NEW.id, NEW.publisher_email);
    RETURN new_arch;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER architecture_publisher
    INSTEAD OF INSERT
    ON arq.publish_architectures_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE publish_architecture_dispatcher_trigger();

-- accept terms

CREATE FUNCTION arq.accept_terms(package_id uuid, parent_taxonomy_id uuid, terms varchar(255)[])
    RETURNS setof arq.packages
AS
$BODY$
DECLARE
    previous_id    uuid;
    core_source_id uuid;
    term           varchar(255);
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;

    -- get taxonomy source id
    SELECT id INTO core_source_id FROM arq.taxonomy_sources WHERE name = 'Core' LIMIT 1;

    -- create first new term
    INSERT INTO arq.taxonomies(term, source_id)
    VALUES (terms[1], core_source_id)
    RETURNING id INTO previous_id;
    IF parent_taxonomy_id IS NOT NULL THEN
        UPDATE arq.taxonomies
        SET parent_id = parent_taxonomy_id
        WHERE id = previous_id;
    END IF;

    -- iterate through remaining terms
    FOREACH term IN ARRAY terms[2:array_length(terms, 1)]
        LOOP
            INSERT INTO arq.taxonomies (term, source_id, parent_id)
            VALUES (term, core_source_id, previous_id)
            RETURNING id INTO previous_id;
        END LOOP;

    -- set package to reference last taxonomy
    RETURN QUERY UPDATE arq.packages
        SET taxonomy_id = previous_id
        WHERE id = package_id
        RETURNING *;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE VIEW arq.accept_terms_mutation_view AS
SELECT *, (SELECT '{}'::varchar(255)[])::varchar(255)[] AS terms
FROM arq.packages;

CREATE FUNCTION accept_terms_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    package    arq.packages;
    empty_list varchar(255)[] := '{}';
BEGIN
    package = arq.accept_terms(NEW.id, NEW.taxonomy_id, NEW.terms);
    RETURN (package.*, empty_list);
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER term_acceptor
    INSTEAD OF INSERT
    ON arq.accept_terms_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE accept_terms_dispatcher_trigger();

-- merge package function/view/trigger

CREATE FUNCTION arq.merge_package(package_id uuid, tax_id uuid, parent uuid, pkg_show boolean)
    RETURNS setof arq.packages
AS
$BODY$
DECLARE
    arch_id uuid;
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;

    -- get arch_id
    SELECT architecture_id
    INTO arch_id
    FROM arq.packages
    WHERE id = package_id;
    IF arch_id IS NULL THEN
        SELECT architecture_id
        INTO arch_id
        FROM arq.packages
        WHERE id = parent;
    END IF;

    -- create new package
    INSERT INTO arq.packages(id, term, taxonomy_id, parent_id, architecture_id, show, sequence)
    VALUES (package_id,
            (SELECT term
             FROM arq.taxonomies
             WHERE id = tax_id),
            tax_id,
            parent,
            arch_id,
            pkg_show,
            COALESCE((SELECT MAX(sequence) FROM arq.packages WHERE parent_id = parent AND deleted_at IS NULL), -1) + 1)
    ON CONFLICT (id)
        DO UPDATE
        SET term                 = (SELECT term
                                    FROM arq.taxonomies
                                    WHERE id = tax_id),
            taxonomy_id          = tax_id,
            reference_package_id = NULL;

    RETURN QUERY SELECT *
                 FROM arq.packages
                 WHERE id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE VIEW arq.merge_packages_mutation_view AS
SELECT *
FROM arq.packages;

CREATE FUNCTION merge_packages_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    package arq.packages;
BEGIN
    package = arq.merge_package(NEW.id, NEW.taxonomy_id, NEW.parent_id, NEW.show);
    RETURN package;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER package_merger
    INSTEAD OF INSERT
    ON arq.merge_packages_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE merge_packages_dispatcher_trigger();

-- delete package function/view/trigger

CREATE FUNCTION arq.delete_package(package_id uuid)
    RETURNS setof arq.packages
AS
$BODY$
BEGIN
    -- soft delete package
    UPDATE arq.packages
    SET deleted_at = NOW()
    WHERE id = package_id;

    RETURN QUERY SELECT * FROM arq.packages WHERE id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE VIEW arq.delete_packages_mutation_view AS
SELECT *
FROM arq.packages;

CREATE FUNCTION delete_packages_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    package arq.packages;
BEGIN
    package = arq.delete_package(OLD.id);
    RETURN package;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER package_deletion
    INSTEAD OF DELETE
    ON arq.delete_packages_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE delete_packages_dispatcher_trigger();

-- append document block function/view/trigger

CREATE FUNCTION arq.insert_architecture(arch_id uuid, email varchar(255))
    RETURNS setof arq.architectures
AS
$BODY$
DECLARE
BEGIN
    -- validate assumptions
    IF arch_id IS NULL THEN
        RAISE EXCEPTION 'null architecture id submitted';
    END IF;
    IF email IS NULL THEN
        RAISE EXCEPTION 'null submitter email submitted';
    END IF;
    IF email = '' THEN
        RAISE EXCEPTION 'empty submitter email submitted';
    END IF;

    -- create new architecture
    INSERT INTO arq.architectures(id, submitter_email)
    VALUES (arch_id, email);

    RETURN QUERY SELECT *
                 FROM arq.architectures
                 WHERE id = arch_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.append_document_block(document_id uuid, arch_id uuid)
    RETURNS setof arq.documents
AS
$BODY$
DECLARE
BEGIN
    -- validate assumptions
    IF document_id IS NULL THEN
        RAISE EXCEPTION 'null document id submitted';
    END IF;
    IF arch_id IS NULL THEN
        RAISE EXCEPTION 'null architecture id submitted';
    END IF;

    -- append new blocks
    UPDATE arq.documents
    SET document_blocks = document_blocks || ARRAY [ROW (NULL, arch_id)::public.block, ROW ('', NULL)::public.block]
    WHERE id = document_id;

    RETURN QUERY SELECT *
                 FROM arq.documents
                 WHERE id = document_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE VIEW arq.append_document_block_mutation_view AS
SELECT *, uuid_generate_v4() AS architecture_id
FROM arq.documents;

CREATE FUNCTION append_document_block_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    architecture arq.architectures;
    document     arq.documents;
BEGIN
    architecture = arq.insert_architecture(NEW.architecture_id, NEW.author_email);
    document = arq.append_document_block(NEW.id, NEW.architecture_id);
    RETURN (document.*, NEW.architecture_id);
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER document_block_appender
    INSTEAD OF INSERT
    ON arq.append_document_block_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE append_document_block_dispatcher_trigger();

-- delete collaboration_invitation function/view/trigger

CREATE FUNCTION arq.delete_collaboration_invitations(collaboration_invitation_id uuid)
    RETURNS setof arq.collaboration_invitations
AS
$BODY$
BEGIN
    -- soft delete collaboration_invitation
    UPDATE arq.collaboration_invitations
    SET updated_at = NOW(),
        deleted_at = NOW()
    WHERE id = collaboration_invitation_id;

    RETURN QUERY SELECT *
                 FROM arq.collaboration_invitations
                 WHERE id = collaboration_invitation_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE VIEW arq.delete_collaboration_invitations_mutation_view AS
SELECT *
FROM arq.collaboration_invitations;


CREATE FUNCTION delete_collaboration_invitations_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    collaboration_invitation arq.collaboration_invitations;
BEGIN
    collaboration_invitation = arq.delete_collaboration_invitations(OLD.id);
    RETURN collaboration_invitation;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER collaboration_invitation_deletion
    INSTEAD OF DELETE
    ON arq.delete_collaboration_invitations_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE delete_collaboration_invitations_dispatcher_trigger();


-- package mutation function/view/trigger

CREATE FUNCTION arq.move_package_up(package_id uuid)
    RETURNS setof arq.packages
AS
$BODY$
DECLARE
    other_package_id uuid;
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;

    -- select other package
    SELECT id
    INTO other_package_id
    FROM arq.packages
    WHERE sequence = (SELECT sequence - 1 FROM arq.packages WHERE id = package_id)
      AND (architecture_id = (SELECT architecture_id FROM arq.packages WHERE id = package_id))
      AND ((parent_id = (SELECT parent_id FROM arq.packages WHERE id = package_id))
        OR ((parent_id IS NULL) AND ((SELECT parent_id FROM arq.packages WHERE id = package_id) IS NULL)))
    LIMIT 1;

    -- make sure the other package exists
    IF other_package_id IS NULL THEN
        RAISE EXCEPTION 'not package to move above';
    END IF;

    -- reorder
    UPDATE arq.packages
    SET sequence = sequence - 1
    WHERE id = package_id;

    UPDATE arq.packages
    SET sequence = sequence + 1
    WHERE id = other_package_id;

    RETURN QUERY SELECT *
                 FROM arq.packages
                 WHERE id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.move_package_down(package_id uuid)
    RETURNS setof arq.packages
AS
$BODY$
DECLARE
    other_package_id uuid;
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;

    -- select other package
    SELECT id
    INTO other_package_id
    FROM arq.packages
    WHERE sequence = (SELECT sequence + 1 FROM arq.packages WHERE id = package_id)
      AND (architecture_id = (SELECT architecture_id FROM arq.packages WHERE id = package_id))
      AND ((parent_id = (SELECT parent_id FROM arq.packages WHERE id = package_id))
        OR ((parent_id IS NULL) AND ((SELECT parent_id FROM arq.packages WHERE id = package_id) IS NULL)))
    LIMIT 1;

    -- make sure the other package exists
    IF other_package_id IS NULL THEN
        RAISE EXCEPTION 'not package to move down';
    END IF;

    -- reorder
    UPDATE arq.packages
    SET sequence = sequence + 1
    WHERE id = package_id;

    UPDATE arq.packages
    SET sequence = sequence - 1
    WHERE id = other_package_id;

    RETURN QUERY SELECT *
                 FROM arq.packages
                 WHERE id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.indent_package(package_id uuid)
    RETURNS setof arq.packages
AS
$BODY$
DECLARE
    new_parent_package_id uuid;
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;

    -- select new parent package
    SELECT id
    INTO new_parent_package_id
    FROM arq.packages
    WHERE sequence = (SELECT sequence - 1 FROM arq.packages WHERE id = package_id)
      AND architecture_id = (SELECT architecture_id FROM arq.packages WHERE id = package_id)
      AND ((parent_id = (SELECT parent_id FROM arq.packages WHERE id = package_id))
        OR ((parent_id IS NULL) AND ((SELECT parent_id FROM arq.packages WHERE id = package_id) IS NULL)))
    LIMIT 1;
    IF new_parent_package_id IS NULL THEN
        RAISE EXCEPTION 'no package available to indent on';
    END IF;

    -- pull other siblings up
    UPDATE arq.packages
    SET sequence = sequence - 1
    WHERE sequence > (SELECT sequence FROM arq.packages WHERE id = package_id)
      AND architecture_id = (SELECT architecture_id FROM arq.packages WHERE id = package_id)
      AND ((parent_id = (SELECT parent_id FROM arq.packages WHERE id = package_id))
        OR ((parent_id IS NULL) AND ((SELECT parent_id FROM arq.packages WHERE id = package_id) IS NULL)));

    -- set new sequence/parent for target package
    UPDATE arq.packages
    SET sequence  = (SELECT COALESCE(max(sequence), -1) + 1 FROM arq.packages WHERE parent_id = new_parent_package_id),
        parent_id = new_parent_package_id
    WHERE id = package_id;

    RETURN QUERY SELECT *
                 FROM arq.packages
                 WHERE id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.unindent_package(package_id uuid)
    RETURNS setof arq.packages
AS
$BODY$
DECLARE
    parent_package_id uuid;
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;

    -- select parent package id
    SELECT parent_id
    INTO parent_package_id
    FROM arq.packages
    WHERE architecture_id = (SELECT architecture_id FROM arq.packages WHERE id = package_id)
      AND ((parent_id = (SELECT parent_id FROM arq.packages WHERE id = package_id))
        OR ((parent_id IS NULL) AND ((SELECT parent_id FROM arq.packages WHERE id = package_id) IS NULL)))
    LIMIT 1;
    IF parent_package_id IS NULL THEN
        RAISE EXCEPTION 'no package parent available to unindent on';
    END IF;

    -- push parent siblings down
    UPDATE arq.packages
    SET sequence = sequence + 1
    WHERE sequence > (SELECT sequence FROM arq.packages WHERE id = parent_package_id)
      AND architecture_id = (SELECT architecture_id FROM arq.packages WHERE id = parent_package_id)
      AND ((parent_id = (SELECT parent_id FROM arq.packages WHERE id = parent_package_id))
        OR ((parent_id IS NULL) AND ((SELECT parent_id FROM arq.packages WHERE id = parent_package_id) IS NULL)));

    -- set new sequence/parent for target package
    UPDATE arq.packages
    SET sequence  = (SELECT sequence + 1 FROM arq.packages WHERE id = parent_package_id),
        parent_id = (SELECT parent_id FROM arq.packages WHERE id = parent_package_id)
    WHERE id = package_id;

    RETURN QUERY SELECT *
                 FROM arq.packages
                 WHERE id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.toggle_package_children(package_id uuid)
    RETURNS SETOF arq.packages
AS
$BODY$
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;

    WITH RECURSIVE children(id, parent_id) AS (
        /* non-recursive section */
        SELECT id, parent_id
        FROM arq.packages
        WHERE id = package_id

        UNION ALL

        /* recursive section */
        SELECT child.id,
               child.parent_id
        FROM arq.packages child
                 JOIN children parent ON parent.id = child.parent_id
    )

        /* toggle show */
    UPDATE arq.packages p
    SET show = NOT show
    FROM children
    WHERE p.id = children.id
      AND p.id != package_id;

    RETURN QUERY
        SELECT *
        FROM arq.packages t
        WHERE t.id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.link_reference_package(package_id uuid, ref_package_id uuid)
    RETURNS setof arq.packages
AS
$BODY$
DECLARE
    reference_package arq.packages%rowtype;
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;
    IF ref_package_id IS NULL THEN
        RAISE EXCEPTION 'null reference package id submitted';
    END IF;

    -- get reference package
    SELECT * INTO reference_package FROM arq.packages WHERE id = ref_package_id;
    IF reference_package IS NULL THEN
        RAISE EXCEPTION 'null reference package selected';
    END IF;

    -- update reference link
    UPDATE arq.packages
    SET reference_package_id = reference_package.id
    WHERE id = package_id;

    -- overwrite taxonomy link if exists
    IF reference_package.taxonomy_id IS NOT NULL THEN
        UPDATE arq.packages
        SET taxonomy_id = reference_package.taxonomy_id
        WHERE id = package_id;
    END IF;

    RETURN QUERY SELECT *
                 FROM arq.packages
                 WHERE id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.insert_package_before(package_id uuid, ref_package_id uuid)
    RETURNS setof arq.packages
AS
$BODY$
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;
    IF ref_package_id IS NULL THEN
        RAISE EXCEPTION 'null target package id submitted';
    END IF;

    -- push down target package and further down siblings
    UPDATE arq.packages
    SET sequence = sequence + 1
    WHERE architecture_id = (SELECT architecture_id FROM arq.packages WHERE id = ref_package_id)
      AND sequence >= (SELECT sequence FROM arq.packages WHERE id = ref_package_id)
      AND ((parent_id = (SELECT parent_id FROM arq.packages WHERE id = ref_package_id))
        OR ((parent_id IS NULL) AND ((SELECT parent_id FROM arq.packages WHERE id = ref_package_id) IS NULL)));

    RETURN QUERY SELECT *
                 FROM arq.packages
                 WHERE id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.insert_package_after(package_id uuid, ref_package_id uuid)
    RETURNS setof arq.packages
AS
$BODY$
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;
    IF ref_package_id IS NULL THEN
        RAISE EXCEPTION 'null target package id submitted';
    END IF;

    -- push down target package and further down siblings
    UPDATE arq.packages
    SET sequence = sequence + 1
    WHERE architecture_id = (SELECT architecture_id FROM arq.packages WHERE id = ref_package_id)
      AND sequence > (SELECT sequence FROM arq.packages WHERE id = ref_package_id)
      AND ((parent_id = (SELECT parent_id FROM arq.packages WHERE id = ref_package_id))
        OR ((parent_id IS NULL) AND ((SELECT parent_id FROM arq.packages WHERE id = ref_package_id) IS NULL)));

    RETURN QUERY SELECT *
                 FROM arq.packages
                 WHERE id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.insert_package_with_sequence(package_id uuid, ref_package_id uuid, ref_position bigint)
    RETURNS setof arq.packages
AS
$BODY$
DECLARE
    reference_package arq.packages%rowtype;
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;
    IF ref_package_id IS NULL THEN
        RAISE EXCEPTION 'null target package id submitted';
    END IF;
    IF ref_position IS NULL THEN
        RAISE EXCEPTION 'null reference package position submitted';
    END IF;

    -- get reference package
    SELECT * INTO reference_package FROM arq.packages WHERE id = ref_package_id;
    IF reference_package IS NULL THEN
        RAISE EXCEPTION 'null target package selected';
    END IF;

    -- insert new package into proper place
    INSERT INTO arq.packages (id, sequence, parent_id, architecture_id)
    VALUES (package_id, reference_package.sequence + ref_position, reference_package.parent_id,
            reference_package.architecture_id);

    RETURN QUERY SELECT *
                 FROM arq.packages
                 WHERE id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TYPE public.package_mutation_action AS enum ('moveUp', 'moveDown', 'indent', 'unindent', 'linkReference', 'insertBefore', 'insertAfter', 'toggleChildren', 'return');

CREATE VIEW arq.package_mutation_view AS
SELECT *, 'return'::public.package_mutation_action AS action
FROM arq.packages;

CREATE FUNCTION mutate_package_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    package arq.packages;
BEGIN
    CASE NEW.action
        WHEN 'moveUp'::public.package_mutation_action THEN package = arq.move_package_up(NEW.id);
        WHEN 'moveDown'::public.package_mutation_action THEN package = arq.move_package_down(NEW.id);
        WHEN 'indent'::public.package_mutation_action THEN package = arq.indent_package(NEW.id);
        WHEN 'unindent'::public.package_mutation_action THEN package = arq.unindent_package(NEW.id);
        WHEN 'linkReference'::public.package_mutation_action
            THEN package = arq.link_reference_package(NEW.id, NEW.reference_package_id);
        WHEN 'insertBefore'::public.package_mutation_action
            THEN package = arq.insert_package_before(NEW.id, NEW.reference_package_id);
                 package = arq.insert_package_with_sequence(NEW.id, NEW.reference_package_id, -1);
        WHEN 'insertAfter'::public.package_mutation_action
            THEN package = arq.insert_package_after(NEW.id, NEW.reference_package_id);
                 package = arq.insert_package_with_sequence(NEW.id, NEW.reference_package_id, 1);
        WHEN 'toggleChildren'::public.package_mutation_action THEN package = arq.toggle_package_children(NEW.id);
        ELSE RAISE EXCEPTION 'unknown package action: %', NEW.action;
        END CASE;

    RETURN (package.*, 'return'::public.package_mutation_action);
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER package_mutator
    INSTEAD OF INSERT
    ON arq.package_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE mutate_package_dispatcher_trigger();

-- delete architecture function/trigger/view

CREATE FUNCTION arq.delete_architecture(architecture_id uuid)
    RETURNS setof arq.architectures
AS
$BODY$
DECLARE
    package_id   uuid;
    document_id  uuid;
    position     int;
    blocks       public.block[];
    block_before public.block;
    block_after  public.block;
    new_block    public.block;
    arch         arq.architectures%rowtype;
BEGIN
    IF architecture_id IS NULL THEN
        RAISE EXCEPTION 'null architecture id provided';
    END IF;

    -- retrieve architecture
    SELECT * INTO arch FROM arq.architectures WHERE id = architecture_id;
    IF arq.architectures_status(arch) != 'draft' THEN
        RAISE EXCEPTION 'invalid architecture status for deletion';
    END IF;

    -- soft delete all packages
    FOR package_id IN SELECT id FROM arq.architectures_package_roots(arch)
        LOOP
            UPDATE arq.packages
            SET updated_at = NOW(),
                deleted_at = NOW()
            WHERE id = package_id;
        END LOOP;

    -- remove blocks that reference this architecture
    SELECT id, document_blocks, array_position(document_blocks, (NULL, architecture_id)::public.block)
    INTO document_id, blocks, position
    FROM arq.documents
    WHERE ROW (NULL, architecture_id)::public.block = ANY (document_blocks);

    block_before = blocks[position - 1];
    block_after = blocks[position + 1];
    new_block.body = concat(block_before.body, block_after.body);

    blocks = array_remove(blocks, (NULL, architecture_id)::public.block);
    blocks = array_remove(blocks, block_after);
    blocks = array_replace(blocks, block_before, new_block);

    UPDATE arq.documents SET document_blocks = blocks WHERE id = document_id;

    -- soft delete architecture
    UPDATE arq.architectures
    SET updated_at = NOW(),
        deleted_at = NOW()
    WHERE id = architecture_id;

    RETURN QUERY SELECT *
                 FROM arq.architectures
                 WHERE id = architecture_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE VIEW arq.delete_architectures_mutation_view AS
SELECT *
FROM arq.architectures;

CREATE FUNCTION arq.delete_architectures_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    architecture arq.architectures;
BEGIN
    architecture = arq.delete_architecture(OLD.id);
    RETURN architecture;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER architecture_deletion
    INSTEAD OF DELETE
    ON arq.delete_architectures_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE arq.delete_architectures_dispatcher_trigger();

-- insert under core taxonomy function/trigger/view

CREATE VIEW arq.insert_core_taxonomies_mutation_view AS
SELECT *
FROM arq.taxonomies;

CREATE FUNCTION arq.insert_core_taxonomy(core_tax_id uuid, ref_tax_id uuid, core_term varchar(255), core_parent_id uuid)
    RETURNS setof arq.taxonomies
AS
$BODY$
DECLARE
    core arq.taxonomy_sources%ROWTYPE;
BEGIN
    -- retrieve core source id
    SELECT *
    INTO core
    FROM arq.taxonomy_sources
    WHERE name = 'Core';

    -- insert core taxonomy
    INSERT INTO arq.taxonomies (id, source_id, term, parent_id)
    VALUES (core_tax_id, core.id, core_term, core_parent_id);

    -- update taxonomy.referencedBy [packages]
    UPDATE arq.packages
    SET taxonomy_id = core_tax_id
    WHERE taxonomy_id = ref_tax_id;

    -- update taxonomy.reference_id [coreTaxonomy and [references]]
    UPDATE arq.taxonomies
    SET reference_id = core_tax_id
    WHERE id = ref_tax_id;

    RETURN QUERY SELECT * FROM arq.taxonomies WHERE id = core_tax_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.insert_core_taxonomy_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    new_tax arq.taxonomies;
BEGIN
    new_tax = arq.insert_core_taxonomy(NEW.id, NEW.reference_id, NEW.term, NEW.parent_id);
    RETURN new_tax;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER core_taxonomy_creation
    INSTEAD OF INSERT
    ON arq.insert_core_taxonomies_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE arq.insert_core_taxonomy_dispatcher_trigger();


-- set taxonomy core taxonomy function/trigger/view

CREATE VIEW arq.set_core_taxonomies_mutation_view AS
SELECT *
FROM arq.taxonomies;

CREATE FUNCTION arq.set_core_taxonomy(tax_id uuid, core_tax_id uuid)
    RETURNS setof arq.taxonomies
AS
$BODY$
BEGIN
    -- update taxonomy.referencedBy [packages]
    UPDATE arq.packages
    SET taxonomy_id = core_tax_id
    WHERE taxonomy_id = tax_id;

    -- set taxonomy core reference
    UPDATE arq.taxonomies
    SET reference_id = core_tax_id
    WHERE id = tax_id;

    RETURN QUERY SELECT * FROM arq.taxonomies WHERE id = tax_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.set_core_taxonomy_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    new_tax arq.taxonomies;
BEGIN
    new_tax = arq.set_core_taxonomy(NEW.id, NEW.reference_id);
    RETURN new_tax;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER core_taxonomy_creation
    INSTEAD OF INSERT
    ON arq.set_core_taxonomies_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE arq.set_core_taxonomy_dispatcher_trigger();


-- updated architecture/document updated_at function/trigger/view

CREATE VIEW arq.update_doc_architecture_mutation_view AS
SELECT *
FROM arq.architectures;

CREATE FUNCTION arq.update_doc_architecture(arch_id uuid)
    RETURNS setof arq.architectures
AS
$BODY$
DECLARE
    document arq.documents%rowtype;
BEGIN
    IF arch_id IS NULL THEN
        RAISE EXCEPTION 'null architecture id provided';
    END IF;

    -- update architecture
    UPDATE arq.architectures
    SET updated_at = NOW()
    WHERE id = arch_id;

    -- get document
    SELECT *
    INTO document
    FROM arq.documents doc,
         unnest(doc.document_blocks) AS b
    WHERE b.architecture_id = arch_id;

    -- update document
    UPDATE arq.documents
    SET updated_at = NOW()
    WHERE id = document.id;

    RETURN QUERY SELECT *
                 FROM arq.architectures
                 WHERE id = arch_id;
END
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.update_doc_architecture_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    arch arq.architectures;
BEGIN
    arch = arq.update_doc_architecture(OLD.id);
    RETURN arch;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER doc_architecture_update
    INSTEAD OF UPDATE
    ON arq.update_doc_architecture_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE arq.update_doc_architecture_dispatcher_trigger();


-- replace package function/view/trigger

CREATE VIEW arq.replace_packages_mutation_view AS
SELECT *
FROM arq.packages;

CREATE FUNCTION arq.soft_delete_linked_package_children(package_id uuid)
    RETURNS SETOF arq.packages
AS
$BODY$
DECLARE
    children arq.packages;
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;

    WITH RECURSIVE children(id, parent_id) AS (
        /* non-recursive section */
        SELECT id, parent_id
        FROM arq.packages
        WHERE id = package_id

        UNION ALL

        /* recursive section */
        SELECT child.id,
               child.parent_id
        FROM arq.packages child
                 JOIN children parent ON parent.id = child.parent_id
    )

        /* soft delete linked package children */
    UPDATE arq.packages p
    SET deleted_at = NOW()
    FROM children c
    WHERE p.id = c.id
      AND p.id != package_id
      AND p.taxonomy_id IS NOT NULL;

    RETURN QUERY
        SELECT *
        FROM arq.packages t
        WHERE t.id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.replace_package(package_id uuid, package_term text)
    RETURNS SETOF arq.packages
AS
$BODY$
BEGIN
    -- validate assumptions
    IF package_id IS NULL THEN
        RAISE EXCEPTION 'null package id submitted';
    END IF;

    -- update package
    UPDATE arq.packages p
    SET term        = package_term,
        link_broken = FALSE
    WHERE p.id = package_id;

    RETURN QUERY
        SELECT *
        FROM arq.packages t
        WHERE t.id = package_id;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE FUNCTION arq.replace_package_dispatcher_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    package       arq.packages;
    pkg_parent_id uuid;
    pkg_show      boolean;
BEGIN
    pkg_parent_id = NEW.parent_id;
    pkg_show = NEW.show;
    IF pkg_parent_id IS NULL THEN
        SELECT INTO pkg_parent_id, pkg_show parent_id,
                                            show
        FROM arq.packages
        WHERE id = NEW.id;
    END IF;

    package = arq.soft_delete_linked_package_children(NEW.id);
    package = arq.merge_package(NEW.id, NEW.taxonomy_id, pkg_parent_id, pkg_show);
    package = arq.replace_package(NEW.id, NEW.term);
    RETURN package;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER package_replace
    INSTEAD OF INSERT
    ON arq.replace_packages_mutation_view
    FOR EACH ROW
EXECUTE PROCEDURE arq.replace_package_dispatcher_trigger();

CREATE FUNCTION arq.taxonomy_for_chain(terms varchar(255)[])
    RETURNS setof arq.taxonomies
AS
$BODY$
DECLARE
    taxonomy arq.taxonomies%rowtype;
    nextTerm varchar(255);
BEGIN
    -- validate assumptions
    IF terms IS NULL THEN
        RAISE EXCEPTION 'null chain submitted submitted';
    END IF;
    IF array_length(terms, 1) = 0 THEN
        RAISE EXCEPTION 'null target package id submitted';
    END IF;

    SELECT *
    INTO taxonomy
    FROM arq.taxonomies
    WHERE term = terms[1]
      AND source_id = (SELECT id FROM arq.taxonomy_sources WHERE name = 'Core')
      AND parent_id IS NULL
    LIMIT 1;
    RAISE INFO 'first: %', taxonomy;

    RAISE INFO 'terms: %', terms[2:];

    FOREACH nextTerm IN ARRAY terms[2:]
        LOOP
            RAISE INFO 'nextTerm: %, parentID: %', terms[2:], taxonomy.id;
            SELECT * INTO taxonomy FROM arq.taxonomies WHERE parent_id = taxonomy.id AND term = nextTerm;
            IF taxonomy IS NULL THEN
                RAISE EXCEPTION 'no taxonomy found for chain value %', nextTerm;
            END IF;
        END LOOP;

    RETURN QUERY SELECT *
                 FROM arq.taxonomies
                 WHERE id = taxonomy.id;
END;
$BODY$
    LANGUAGE plpgsql STABLE;
