CREATE OR REPLACE FUNCTION arq.transfer_package_tree(arch_id uuid, prnt_id uuid, package arq.packages)
    RETURNS setof arq.packages
AS
$$
DECLARE
    new_id uuid;
    child  arq.packages%rowtype;
BEGIN
    new_id = public.uuid_generate_v4();

    IF package.deleted_at IS NULL THEN
        PERFORM arq.publish_insert_package(new_id, prnt_id, arch_id, package);

    FOR child IN SELECT * FROM arq.packages WHERE parent_id = package.id
        LOOP
            PERFORM arq.transfer_package_tree(arch_id, new_id, child);
        END LOOP;

    RETURN QUERY SELECT * FROM arq.packages WHERE id = new_id;
END;
$$
    LANGUAGE plpgsql VOLATILE;