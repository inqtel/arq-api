ALTER TABLE arq.taxonomies
    ADD COLUMN exported_at timestamp without time zone;
