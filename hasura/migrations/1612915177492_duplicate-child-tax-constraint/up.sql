CREATE UNIQUE INDEX taxonomies_term_parent_id_key
    ON arq.taxonomies (lower(term), parent_id);