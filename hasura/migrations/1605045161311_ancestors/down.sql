CREATE OR REPLACE FUNCTION arq.taxonomies_ancestors(taxonomy arq.taxonomies)
    RETURNS SETOF arq.taxonomies
AS
$$
BEGIN
    RETURN QUERY
        SELECT *
        FROM arq.taxonomies t
        WHERE t.id IN (WITH RECURSIVE ancestors(id, parent_id) AS (
            /* non-recursive section */
            SELECT a.id, a.parent_id
            FROM arq.taxonomies a
            WHERE a.id = taxonomy.id
            UNION ALL
            /* recursive section */
            SELECT parent.id, parent.parent_id
            FROM arq.taxonomies parent
                     JOIN ancestors children ON children.parent_id = parent.id)
                       SELECT id
                       FROM ancestors);
END
$$
    LANGUAGE plpgsql STABLE
                     STRICT;