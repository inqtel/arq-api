CREATE OR REPLACE FUNCTION arq.taxonomies_ancestors(taxonomy arq.taxonomies) RETURNS SETOF arq.taxonomies
    STABLE
    STRICT
    LANGUAGE plpgsql
AS
$$
DECLARE
    tid uuid;
BEGIN
    FOR tid IN SELECT id
               FROM (WITH RECURSIVE ancestors(id, parent_id) AS (
                   /* non-recursive section */
                   SELECT a.id, a.parent_id, 0 AS level
                   FROM arq.taxonomies a
                   WHERE a.id = taxonomy.id
                   UNION ALL
                   /* recursive section */
                   SELECT parent.id, parent.parent_id, level + 1
                   FROM arq.taxonomies parent
                            JOIN ancestors children ON children.parent_id = parent.id)
                     SELECT id
                     FROM ancestors
                     ORDER BY level DESC) ancs
        LOOP
            RETURN QUERY SELECT * FROM arq.taxonomies t WHERE t.id = tid;
        END LOOP;
END
$$;