CREATE OR REPLACE VIEW arq.insert_core_taxonomies_mutation_view AS
SELECT *
FROM arq.taxonomies;

CREATE OR REPLACE VIEW arq.set_core_taxonomies_mutation_view AS
SELECT *
FROM arq.taxonomies;
