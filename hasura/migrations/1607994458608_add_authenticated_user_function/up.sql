CREATE FUNCTION arq.authenticated_user(hasura_session json)
 RETURNS SETOF arq.authenticated_user_view
 LANGUAGE sql
 STABLE
AS $function$
    SELECT *, CASE
      WHEN hasura_session ->> 'x-hasura-role' = 'api-admin'
      THEN TRUE
      ELSE FALSE
      END AS admin 
    FROM arq.users
    WHERE email = hasura_session ->> 'x-hasura-remote-user'
$function$;
