CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE SCHEMA arq;
CREATE TYPE public.block AS
(
    body            text,
    architecture_id uuid
);
CREATE TABLE arq.architectures
(
    id              uuid                        DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at      timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    updated_at      timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    draft_at        timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    submitted_at    timestamp WITHOUT TIME ZONE,
    in_review_at    timestamp WITHOUT TIME ZONE,
    deleted_at      timestamp WITHOUT TIME ZONE,
    rejected_at     timestamp WITHOUT TIME ZONE,
    published_at    timestamp WITHOUT TIME ZONE,
    title           character varying(255),
    submitter_email varchar(255)                                                  NOT NULL,
    publisher_email varchar(255),
    next_version_id uuid
);
CREATE TABLE arq.collaboration_invitations
(
    id                 uuid                        DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at         timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    updated_at         timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    deleted_at         timestamp WITHOUT TIME ZONE,
    seen_at            timestamp WITHOUT TIME ZONE,
    document_id        uuid                                                          NOT NULL,
    collaborator_email varchar(255)                                                  NOT NULL,
    sender_email       varchar(255)                                                  NOT NULL
);
CREATE TABLE arq.customers
(
    id          uuid                        DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at  timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    updated_at  timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    deleted_at  timestamp WITHOUT TIME ZONE,
    name        character varying(255)                                        NOT NULL,
    description text,
    CONSTRAINT customers_name_check CHECK (((name)::text <> ''::text))
);
CREATE TABLE arq.documents
(
    id              uuid                        DEFAULT public.uuid_generate_v4()                            NOT NULL,
    created_at      timestamp WITHOUT TIME ZONE DEFAULT now()                                                NOT NULL,
    updated_at      timestamp WITHOUT TIME ZONE DEFAULT now()                                                NOT NULL,
    deleted_at      timestamp WITHOUT TIME ZONE,
    title           character varying(255),
    author_email    varchar(255)                                                                             NOT NULL,
    document_blocks public.block[]              DEFAULT ARRAY [ROW ('', NULL)::public.block]::public.block[] NOT NULL
);
CREATE TABLE arq.organization_taxonomies
(
    organization_id character varying(255) NOT NULL,
    taxonomy_id     uuid                   NOT NULL
);
CREATE TABLE arq.organizations
(
    id           character varying(255)                    NOT NULL,
    created_at   timestamp WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    updated_at   timestamp WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    synced_at    timestamp WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    deleted_at   timestamp WITHOUT TIME ZONE,
    name         character varying(255)                    NOT NULL,
    pitchbook_id character varying(255),
    logo         character varying(255),
    portfolio    boolean                     DEFAULT FALSE NOT NULL,
    CONSTRAINT organizations_name_check CHECK (((name)::text <> ''::text))
);
CREATE TABLE arq.package_annotations
(
    id              uuid   DEFAULT public.uuid_generate_v4() NOT NULL,
    x               bigint DEFAULT 0,
    y               bigint DEFAULT 0,
    width           bigint DEFAULT 0,
    height          bigint DEFAULT 0,
    rotation        bigint DEFAULT 0,
    package_id      uuid                                     NOT NULL,
    organization_id character varying(255)
);
CREATE TABLE arq.package_comments
(
    id           uuid                        DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at   timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    updated_at   timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    deleted_at   timestamp WITHOUT TIME ZONE,
    resolved_at  timestamp WITHOUT TIME ZONE,
    text         text,
    package_id   uuid                                                          NOT NULL,
    author_email varchar(255)                                                  NOT NULL
);
CREATE TABLE arq.packages
(
    id                     uuid                        DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at             timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    updated_at             timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    deleted_at             timestamp WITHOUT TIME ZONE,
    term                   character varying(255)      DEFAULT ''                        NOT NULL,
    description            text,
    link_broken            boolean                     DEFAULT FALSE                     NOT NULL,
    sequence               bigint                      DEFAULT 0                         NOT NULL,
    show                   boolean                     DEFAULT TRUE                      NOT NULL,
    level_0_position       bigint                      DEFAULT 0                         NOT NULL,
    level_0_title_position bigint                      DEFAULT 0                         NOT NULL,
    level_1_x              bigint                      DEFAULT 0                         NOT NULL,
    level_1_y              bigint                      DEFAULT 0                         NOT NULL,
    level_1_width          bigint                      DEFAULT 0                         NOT NULL,
    level_1_height         bigint                      DEFAULT 0                         NOT NULL,
    level_1_drawn          boolean                     DEFAULT FALSE                     NOT NULL,
    level_1_hide_children  boolean                     DEFAULT FALSE                     NOT NULL,
    level_1_columns        bigint                      DEFAULT 0                         NOT NULL,
    level_1_position       bigint                      DEFAULT 0                         NOT NULL,
    level_1_title_position bigint                      DEFAULT 0                         NOT NULL,
    level_2_position       bigint                      DEFAULT 0                         NOT NULL,
    taxonomy_id            uuid,
    architecture_id        uuid                                                          NOT NULL,
    parent_id              uuid,
    reference_package_id   uuid
);
CREATE TABLE arq.problem_set_taxonomies
(
    problem_set_id character varying(255) NOT NULL,
    taxonomy_id    uuid                   NOT NULL
);
CREATE TABLE arq.problem_sets
(
    id                character varying(255)                    NOT NULL,
    created_at        timestamp WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    updated_at        timestamp WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    synced_at         timestamp WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    deleted_at        timestamp WITHOUT TIME ZONE,
    number            character varying(255),
    customer_number   character varying(255)                    NOT NULL,
    origination_year  smallint,
    fiscal_year       smallint                                  NOT NULL,
    description       text,
    priority          character varying(255)                    NOT NULL,
    category_of_need  character varying(255)                    NOT NULL,
    customer_category character varying(255)                    NOT NULL,
    office            character varying(255)                    NOT NULL,
    customer_id       uuid                                      NOT NULL
);
CREATE TABLE arq.taxonomies
(
    id           uuid                        DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at   timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    updated_at   timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    deleted_at   timestamp WITHOUT TIME ZONE,
    term         character varying(255)                                        NOT NULL,
    parent_id    uuid,
    source_id    uuid                                                          NOT NULL,
    reference_id uuid
);
CREATE TABLE arq.taxonomy_sources
(
    id         uuid                        DEFAULT public.uuid_generate_v4() NOT NULL,
    created_at timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    updated_at timestamp WITHOUT TIME ZONE DEFAULT now()                     NOT NULL,
    deleted_at timestamp WITHOUT TIME ZONE,
    name       character varying(30)                                         NOT NULL
);
CREATE TABLE arq.text_result
(
    result text
);
CREATE TABLE arq.users
(
    email      varchar(255)                              NOT NULL,
    created_at timestamp WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    updated_at timestamp WITHOUT TIME ZONE DEFAULT now() NOT NULL,
    deleted_at timestamp WITHOUT TIME ZONE,
    admin      boolean                     DEFAULT FALSE NOT NULL,
    CONSTRAINT users_email_check CHECK (((email)::text <> ''::text))
);
CREATE TABLE arq.constants
(
    key   varchar(255) NOT NULL,
    value varchar(255) NOT NULL
);
ALTER TABLE ONLY arq.constants
    ADD CONSTRAINT constants_pkey PRIMARY KEY (key);
ALTER TABLE ONLY arq.architectures
    ADD CONSTRAINT architectures_pkey PRIMARY KEY (id);
ALTER TABLE ONLY arq.collaboration_invitations
    ADD CONSTRAINT collaboration_invitations_document_id_collaborator_email_key UNIQUE (document_id, collaborator_email);
ALTER TABLE ONLY arq.collaboration_invitations
    ADD CONSTRAINT collaboration_invitations_pkey PRIMARY KEY (id);
ALTER TABLE ONLY arq.customers
    ADD CONSTRAINT customers_name_key UNIQUE (name);
ALTER TABLE ONLY arq.customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);
ALTER TABLE ONLY arq.documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);
ALTER TABLE ONLY arq.organization_taxonomies
    ADD CONSTRAINT organization_taxonomies_pkey PRIMARY KEY (organization_id, taxonomy_id);
ALTER TABLE ONLY arq.organizations
    ADD CONSTRAINT organizations_pkey PRIMARY KEY (id);
ALTER TABLE ONLY arq.package_annotations
    ADD CONSTRAINT package_annotations_pkey PRIMARY KEY (id);
ALTER TABLE ONLY arq.package_comments
    ADD CONSTRAINT package_comments_pkey PRIMARY KEY (id);
ALTER TABLE ONLY arq.packages
    ADD CONSTRAINT packages_pkey PRIMARY KEY (id);
ALTER TABLE ONLY arq.problem_set_taxonomies
    ADD CONSTRAINT problem_set_taxonomies_pkey PRIMARY KEY (problem_set_id, taxonomy_id);
ALTER TABLE ONLY arq.problem_sets
    ADD CONSTRAINT problem_sets_pkey PRIMARY KEY (id);
ALTER TABLE ONLY arq.taxonomies
    ADD CONSTRAINT taxonomies_pkey PRIMARY KEY (id);
ALTER TABLE ONLY arq.taxonomy_sources
    ADD CONSTRAINT taxonomy_sources_pkey PRIMARY KEY (id);
ALTER TABLE ONLY arq.taxonomy_sources
    ADD CONSTRAINT taxonomy_sources_name_key UNIQUE (name);
ALTER TABLE ONLY arq.users
    ADD CONSTRAINT users_email_key UNIQUE (email);
ALTER TABLE ONLY arq.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (email);
ALTER TABLE ONLY arq.architectures
    ADD CONSTRAINT architectures_next_version_id_fkey FOREIGN KEY (next_version_id) REFERENCES arq.architectures (id) ON DELETE SET NULL;
ALTER TABLE ONLY arq.architectures
    ADD CONSTRAINT architectures_publisher_email_fkey FOREIGN KEY (publisher_email) REFERENCES arq.users (email) ON DELETE SET NULL;
ALTER TABLE ONLY arq.architectures
    ADD CONSTRAINT architectures_submitter_email_fkey FOREIGN KEY (submitter_email) REFERENCES arq.users (email) ON DELETE CASCADE;
ALTER TABLE ONLY arq.collaboration_invitations
    ADD CONSTRAINT collaboration_invitations_collaborator_email_fkey FOREIGN KEY (collaborator_email) REFERENCES arq.users (email) ON DELETE CASCADE;
ALTER TABLE ONLY arq.collaboration_invitations
    ADD CONSTRAINT collaboration_invitations_document_id_fkey FOREIGN KEY (document_id) REFERENCES arq.documents (id) ON DELETE CASCADE;
ALTER TABLE ONLY arq.collaboration_invitations
    ADD CONSTRAINT collaboration_invitations_sender_email_fkey FOREIGN KEY (sender_email) REFERENCES arq.users (email) ON DELETE CASCADE;
ALTER TABLE ONLY arq.documents
    ADD CONSTRAINT documents_author_email_fkey FOREIGN KEY (author_email) REFERENCES arq.users (email) ON DELETE CASCADE;
ALTER TABLE ONLY arq.organization_taxonomies
    ADD CONSTRAINT organization_taxonomies_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES arq.organizations (id) ON DELETE CASCADE;
ALTER TABLE ONLY arq.organization_taxonomies
    ADD CONSTRAINT organization_taxonomies_taxonomy_id_fkey FOREIGN KEY (taxonomy_id) REFERENCES arq.taxonomies (id) ON DELETE CASCADE;
ALTER TABLE ONLY arq.package_annotations
    ADD CONSTRAINT package_annotations_organization_id_fkey FOREIGN KEY (organization_id) REFERENCES arq.organizations (id) ON DELETE SET NULL;
ALTER TABLE ONLY arq.package_annotations
    ADD CONSTRAINT package_annotations_package_id_fkey FOREIGN KEY (package_id) REFERENCES arq.packages (id) ON DELETE CASCADE;
ALTER TABLE ONLY arq.package_comments
    ADD CONSTRAINT package_comments_author_email_fkey FOREIGN KEY (author_email) REFERENCES arq.users (email) ON DELETE CASCADE;
ALTER TABLE ONLY arq.package_comments
    ADD CONSTRAINT package_comments_package_id_fkey FOREIGN KEY (package_id) REFERENCES arq.packages (id) ON DELETE CASCADE;
ALTER TABLE ONLY arq.packages
    ADD CONSTRAINT packages_architecture_id_fkey FOREIGN KEY (architecture_id) REFERENCES arq.architectures (id) ON DELETE CASCADE;
ALTER TABLE ONLY arq.packages
    ADD CONSTRAINT packages_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES arq.packages (id);
ALTER TABLE ONLY arq.packages
    ADD CONSTRAINT packages_reference_package_id_fkey FOREIGN KEY (reference_package_id) REFERENCES arq.packages (id) ON DELETE SET NULL;
ALTER TABLE ONLY arq.packages
    ADD CONSTRAINT packages_taxonomy_id_fkey FOREIGN KEY (taxonomy_id) REFERENCES arq.taxonomies (id) ON DELETE SET NULL;
ALTER TABLE ONLY arq.problem_set_taxonomies
    ADD CONSTRAINT problem_set_taxonomies_problem_set_id_fkey FOREIGN KEY (problem_set_id) REFERENCES arq.problem_sets (id) ON DELETE CASCADE;
ALTER TABLE ONLY arq.problem_set_taxonomies
    ADD CONSTRAINT problem_set_taxonomies_taxonomy_id_fkey FOREIGN KEY (taxonomy_id) REFERENCES arq.taxonomies (id) ON DELETE CASCADE;
ALTER TABLE ONLY arq.problem_sets
    ADD CONSTRAINT problem_sets_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES arq.customers (id) ON DELETE CASCADE;
ALTER TABLE ONLY arq.taxonomies
    ADD CONSTRAINT taxonomies_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES arq.taxonomies (id) ON DELETE SET NULL;
ALTER TABLE ONLY arq.taxonomies
    ADD CONSTRAINT taxonomies_reference_id_fkey FOREIGN KEY (reference_id) REFERENCES arq.taxonomies (id) ON DELETE SET NULL;
ALTER TABLE ONLY arq.taxonomies
    ADD CONSTRAINT taxonomies_source_id_fkey FOREIGN KEY (source_id) REFERENCES arq.taxonomy_sources (id) ON DELETE CASCADE;
