CREATE VIEW arq.authenticated_user_view AS
  SELECT *, (SELECT FALSE)::boolean AS admin
  FROM arq.users;
