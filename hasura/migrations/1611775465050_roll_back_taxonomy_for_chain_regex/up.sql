CREATE OR REPLACE FUNCTION arq.taxonomy_for_chain(terms varchar(255)[])
    RETURNS setof arq.taxonomies
AS
$BODY$
DECLARE
    taxonomy arq.taxonomies%rowtype;
    nextTerm varchar(255);
BEGIN
    -- validate assumptions
    IF terms IS NULL THEN
        RAISE EXCEPTION 'null chain submitted submitted';
    END IF;
    IF array_length(terms, 1) = 0 THEN
        RAISE EXCEPTION 'null target package id submitted';
    END IF;

    /* lower() ignores indices, and is slower. if this query becomes too slow, we should look into lowercase indices or citext */

    SELECT *
    INTO taxonomy
    FROM arq.taxonomies
    WHERE lower(term) = lower(terms[1])
      AND source_id = (SELECT id FROM arq.taxonomy_sources WHERE name = 'Core')
      AND parent_id IS NULL
    LIMIT 1;
    RAISE INFO 'first: %', taxonomy;
    RAISE INFO 'terms: %', terms[2:];

    FOREACH nextTerm IN ARRAY terms[2:]
        LOOP
            RAISE INFO 'nextTerm: %, parentID: %', terms[2:], taxonomy.id;
            SELECT * INTO taxonomy FROM arq.taxonomies WHERE parent_id = taxonomy.id AND lower(term) = lower(nextTerm);
            IF taxonomy IS NULL THEN
                RAISE EXCEPTION 'no taxonomy found for chain value %', nextTerm;
            END IF;
        END LOOP;

    RETURN QUERY SELECT *
                 FROM arq.taxonomies
                 WHERE id = taxonomy.id;
END;
$BODY$
    LANGUAGE plpgsql STABLE;
