CREATE FUNCTION arq.user_admin(u arq.users, hasura_session json)
 RETURNS boolean
 LANGUAGE sql
 STABLE
AS $function$
SELECT CASE
    WHEN hasura_session ->> 'x-hasura-role' = 'api-admin' AND 
    hasura_session ->> 'x-hasura-remote-user' = u.email
    THEN TRUE
    ELSE FALSE
    END AS admin 
    FROM arq.users
    WHERE email = u.email
$function$;
