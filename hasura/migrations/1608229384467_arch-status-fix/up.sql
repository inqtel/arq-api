CREATE OR REPLACE FUNCTION arq.architectures_status(architecture arq.architectures)
    RETURNS text
AS
$$
DECLARE
    latest timestamp;
    status text;
BEGIN
    latest = architecture.draft_at;
    status = 'draft';

    IF (architecture.in_review_at IS NOT NULL) AND (architecture.in_review_at > latest) THEN
        latest = architecture.in_review_at;
        status = 'inreview';
    END IF;

    IF (architecture.submitted_at IS NOT NULL) AND (architecture.submitted_at > latest) THEN
        latest = architecture.submitted_at;
        status = 'submitted';
    END IF;

    IF (architecture.rejected_at IS NOT NULL) AND (architecture.rejected_at > latest) THEN
        latest = architecture.rejected_at;
        status = 'rejected';
    END IF;

    IF (architecture.published_at IS NOT NULL) AND (architecture.published_at > latest) THEN
        latest = architecture.published_at;
        status = 'published';
    END IF;

    RETURN status;
END;
$$
    LANGUAGE plpgsql STABLE;