INSERT INTO arq.problem_set_taxonomies (problem_set_id, taxonomy_id) VALUES ('testproblemset1', 'e1fa352b-a1d5-4fa2-bcc7-4da2e0d0f43d');
INSERT INTO arq.problem_set_taxonomies (problem_set_id, taxonomy_id) VALUES ('testproblemset2', 'e1fa352b-a1d5-4fa2-bcc7-4da2e0d0f43d');
INSERT INTO arq.problem_set_taxonomies (problem_set_id, taxonomy_id) VALUES ('testproblemset2', 'a95a2e4c-ed8c-4096-a7c8-2299f0f91360');

INSERT INTO arq.problem_sets (id, created_at, updated_at, synced_at, deleted_at, number, customer_number, origination_year, fiscal_year, description, priority, category_of_need, customer_category, office, customer_id) VALUES ('testproblemset3', '2020-11-02 19:08:22.794273', '2020-11-02 19:08:22.794273', '2020-11-02 19:08:22.794273', NULL, NULL, '1.2.5', NULL, 2020, 'nacho business', 'Game changing', 'Blue', 'Yellow', 'ATL', '9c0f2b59-8f8c-4671-bd5f-1b0894f7bd09');
INSERT INTO arq.problem_sets (id, created_at, updated_at, synced_at, deleted_at, number, customer_number, origination_year, fiscal_year, description, priority, category_of_need, customer_category, office, customer_id) VALUES ('testproblemset4', '2020-11-02 19:08:22.794273', '2020-11-02 19:08:22.794273', '2020-11-02 19:08:22.794273', NULL, NULL, '1.2.6', NULL, 2020, 'nacho labs', 'Low', 'Odds', 'Evens', 'CMH', '9c0f2b59-8f8c-4671-bd5f-1b0894f7bd09');

INSERT INTO arq.problem_set_taxonomies (problem_set_id, taxonomy_id) VALUES ('testproblemset3', 'a81a2ec7-3519-46c8-839a-79ca44369125');
INSERT INTO arq.problem_set_taxonomies (problem_set_id, taxonomy_id) VALUES ('testproblemset4', 'a81a2ec7-3519-46c8-839a-79ca44369125');
INSERT INTO arq.problem_set_taxonomies (problem_set_id, taxonomy_id) VALUES ('testproblemset4', '298435b4-0446-4fff-afd6-335c667308c6');
INSERT INTO arq.problem_set_taxonomies (problem_set_id, taxonomy_id) VALUES ('testproblemset4', 'a95a2e4c-ed8c-4096-a7c8-2299f0f91360');

