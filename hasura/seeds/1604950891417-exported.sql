UPDATE arq.taxonomies
SET exported_at = now()
WHERE source_id = (SELECT id FROM arq.taxonomy_sources WHERE name = 'Core');