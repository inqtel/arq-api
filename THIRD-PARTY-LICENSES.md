We seeded ArQ with three publicly-available taxonomies which are licensed as follows.
1. ACM Computing Classification System: https://www.acm.org/publications/class-2012
    1. The full 2012 ACM Computing Classification System tree is freely available for educational and research purposes in HTML format.
2. IEEE Taxonomy: https://www.ieee.org/publications/services/thesaurus-access-page.html
    1. The IEEE Thesaurus and Taxonomy are copyright by IEEE under CC BY-NC-ND 4.0
3. PLOS Thesaurus: https://github.com/PLOS/plos-thesaurus
    1. PLOS distributes the thesaurus work under CC0 “no rights reserved” (http://creativecommons.org/about/cc0)