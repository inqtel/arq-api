# Welcome

Thank you for considering contributing to ArQ. 

We created these guidelines to make contributing as easy as possible. 

There are many ways to contribute, whether it's shooting us a note to tell us how you are using the ArQ tool, linking to our repository in your tutorials and blog posts, submitting bugs or feature requests, improving documentation, or adding your code to the ArQ codebase.

Any of these will give us some warm fuzzies.


# Ground Rules
* Be respectful. See [Contributor Covenant Code of Conduct](https://www.contributor-covenant.org/version/2/0/code_of_conduct/).
* Ensure that new code meets these requirements: 
  * API successfully [builds and runs locally](https://gitlab.com/inqtel/arq-api#build-and-run-api)
  * Jobs Pipeline successful
  * Merge request [links](https://about.gitlab.com/blog/2016/03/08/gitlab-tutorial-its-all-connected/) to issue it's resolving.


# Getting started

To begin, [set up your SSH keys](https://www.youtube.com/watch?v=0z28J0RfaJM) and [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) the ArQ [client](https://gitlab.com/inqtel/arq-client) and [API](https://gitlab.com/inqtel/arq-client) repositories.

Follow the Developer Quickstart to start both [client](https://gitlab.com/inqtel/arq-client#developer-quickstart-mac-osx) and [API](https://gitlab.com/inqtel/arq-api#developer-quickstart-mac-osx).

Check out the [issues](https://gitlab.com/inqtel/arq-api/-/issues) we've listed for you and choose one to solve. [Create a new branch off of the issue](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch-from-an-issue) and check it out locally. 

Write code.

After you ensure your new code meets our requirements above, create a [merge request](https://www.youtube.com/watch?v=Ddd3dbl4-2w) and assign to a maintainer. A maintainer will get back to you within a day or two, unless it's a holiday or weekend.
