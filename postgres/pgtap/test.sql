CREATE EXTENSION IF NOT EXISTS pgtap;
-- Start transaction and plan the tests.
BEGIN;
SELECT plan(7);

-- Run the tests.
SELECT has_table( 'arq', 'users', 'has users table' );
SELECT columns_are( 'arq', 'users', ARRAY[ 'email', 'created_at', 'updated_at', 'deleted_at', 'admin' ] );
SELECT has_function( 'arq', 'taxonomies_lower', ARRAY[ 'arq.taxonomies' ], 'has arq.taxonomies_lower function' );

-- run tests against custom mutations

-- arq.update_doc_architecture(arch_id uuid)

SELECT lives_ok(
  format($$
      SELECT arq.update_doc_architecture('95a5db6b-6ce7-4676-842e-9cfc642f69e3')
  $$),
  'arq.update_doc_architecture(uuid) function does not trigger an error'
);

SELECT throws_ok(
  format($$
      SELECT arq.update_doc_architecture(null)
  $$),
  'P0001',
  'null architecture id provided',
  'arq.update_doc_architecture(null) throws error when architecture id is null'
);

SELECT * INTO arch_before
    FROM arq.architectures
    WHERE id = '95a5db6b-6ce7-4676-842e-9cfc642f69e3';

SELECT cmp_ok(
    format($$
      SELECT a.updated_at, doc.updated_at
      FROM arq.update_doc_architecture('95a5db6b-6ce7-4676-842e-9cfc642f69e3') a
    $$),
    '>',
    format($$
      SELECT arch_before.updated_at FROM arch_before
    $$),
    'arq.update_doc_architecture function updates architecture.updatedAt field' );


SELECT * INTO document_before
    FROM arq.documents doc, unnest(doc.document_blocks) AS b
    WHERE b.architecture_id = '95a5db6b-6ce7-4676-842e-9cfc642f69e3';

SELECT cmp_ok(
    format($$
      SELECT doc.updated_at FROM arq.update_doc_architecture('95a5db6b-6ce7-4676-842e-9cfc642f69e3') a,
      arq.documents doc, unnest(doc.document_blocks) AS b
      WHERE b.architecture_id = '95a5db6b-6ce7-4676-842e-9cfc642f69e3'
    $$),
    '>',
    format($$
      SELECT document_before.updated_at FROM document_before
    $$),
    'arq.update_doc_architecture function updates document.updatedAt field' );


-- Finish the tests and clean up.
SELECT * FROM finish();
ROLLBACK;