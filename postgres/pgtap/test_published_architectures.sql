CREATE EXTENSION IF NOT EXISTS pgtap;

BEGIN;
SELECT plan(1);

PREPARE pub_arch_query AS
    SELECT * from arq.published_architectures();
select performs_ok(
    'pub_arch_query',
    250,
    'verifies that the published architectures query operates with reasonable performance');

select * from finish() rollback;