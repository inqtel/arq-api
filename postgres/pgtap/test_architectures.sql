CREATE EXTENSION IF NOT EXISTS pgtap;

BEGIN;
SELECT plan(1);

-- create test taxonomies

CREATE OR REPLACE FUNCTION setup_insert(
) RETURNS SETOF TEXT AS
$$
DECLARE
    arch_id uuid;
BEGIN
    SELECT PUBLIC.uuid_generate_v5(uuid_ns_url(), 'arch1') INTO arch_id;
    PERFORM arq.insert_architecture(arch_id, 'mcurie@example.com');
    UPDATE arq.architectures SET submitted_at = now() WHERE id = arch_id;
    UPDATE arq.architectures SET in_review_at = now() WHERE id = arch_id;
    PERFORM arq.publish_architecture(arch_id, 'mcurie@example.com');
END
$$
    LANGUAGE plpgsql;


-- tests
SELECT setup_insert();
PREPARE archs_query AS
    SELECT (SELECT id FROM arq.latest_architecture(a) id)
    FROM arq.architectures a
    WHERE a.id = PUBLIC.uuid_generate_v5(uuid_ns_url(), 'arch1');
PREPARE arch_id AS SELECT next_version_id
                   FROM arq.architectures
                   WHERE id = PUBLIC.uuid_generate_v5(uuid_ns_url(), 'arch1');
SELECT results_eq(
               'archs_query',
               'arch_id',
               'match latest architecture');

-- finish and cleanup
SELECT *
FROM finish();
ROLLBACK;