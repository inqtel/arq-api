CREATE EXTENSION IF NOT EXISTS pgtap;

BEGIN;
SELECT plan(1);

-- create test taxonomies

CREATE OR REPLACE FUNCTION setup_insert(
) RETURNS SETOF TEXT AS
$$
BEGIN
    INSERT INTO arq.taxonomies (source_id, term, id)
    VALUES ((SELECT id FROM arq.taxonomy_sources WHERE NAME = 'Core'), 'first',
            PUBLIC.uuid_generate_v5(uuid_ns_url(), '1'));
    INSERT INTO arq.taxonomies (source_id, term, id, parent_id)
    VALUES ((SELECT id FROM arq.taxonomy_sources WHERE NAME = 'Core'), 'second',
            PUBLIC.uuid_generate_v5(uuid_ns_url(), '2'),
            PUBLIC.uuid_generate_v5(uuid_ns_url(), '1'));
    INSERT INTO arq.taxonomies (source_id, term, id, parent_id)
    VALUES ((SELECT id FROM arq.taxonomy_sources WHERE NAME = 'Core'), 'third',
            PUBLIC.uuid_generate_v5(uuid_ns_url(), '3'),
            PUBLIC.uuid_generate_v5(uuid_ns_url(), '2'));
    INSERT INTO arq.taxonomies (source_id, term, id, parent_id)
    VALUES ((SELECT id FROM arq.taxonomy_sources WHERE NAME = 'Core'), 'fourth',
            PUBLIC.uuid_generate_v5(uuid_ns_url(), '4'),
            PUBLIC.uuid_generate_v5(uuid_ns_url(), '3'));
END
$$
    LANGUAGE plpgsql;


-- tests
SELECT setup_insert();
PREPARE ancs_query AS SELECT unnest((SELECT array_agg(id) FROM arq.taxonomies_ancestors(t) id)) ids
                      FROM arq.taxonomies t
                      WHERE term = 'fourth';
SELECT results_eq(
               'ancs_query',
               ARRAY [PUBLIC.uuid_generate_v5(uuid_ns_url(), '1'),
                   PUBLIC.uuid_generate_v5(uuid_ns_url(), '2'),
                   PUBLIC.uuid_generate_v5(uuid_ns_url(), '3'),
                   PUBLIC.uuid_generate_v5(uuid_ns_url(), '4')],
               'match ancestor results and order');

-- finish and cleanup
SELECT *
FROM finish();
ROLLBACK;