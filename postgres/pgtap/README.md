### Steps to add a pgtap unit test

1. [Start ArQ API](https://gitlab.iqt.org/dev/arq/arq-api#developer-quickstart-mac-osx)

2. [Load seed data](https://gitlab.iqt.org/dev/arq/arq-api#load-seed-data)

3. Open editor of your choice and edit `postgres/pgtap/test.sql` file

4. Run tests via terminal

```console
docker exec -it arq-api_postgres_1 /bin/bash -c "export PGPASSWORD=testpass && pg_prove -U testuser  -h postgres -d arq tests/*.sql"
```