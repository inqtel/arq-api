# bash

echo "taking down services"
docker-compose down --rmi all -v
echo "rebuilding"
docker-compose build
docker-compose up --no-start

echo "bringing up services"
docker-compose start postgres elastic dejavu hasura webhook

echo "waiting for hasura"
until [[ "$(curl --silent --fail http://localhost:8081/v1/version)" == *"version"* ]]; do
    printf '.'
    sleep 5
done
echo "applying seeds"
cd hasura/
hasura seeds apply
cd ..

echo "starting logstash"
docker-compose start logstash