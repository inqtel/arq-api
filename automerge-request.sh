#!/bin/sh
# creates MR from `production` to `public-production` when `production` is updated
# creates MR from `public-production` to `development` when `public-production` is updated

if [ $CI_COMMIT_REF_NAME == "production" ]; then
  TARGET_BRANCH="public-production"
elif [ $CI_COMMIT_REF_NAME == "public-production" ]; then
  TARGET_BRANCH="development"
fi

TITLE="${CI_COMMIT_REF_NAME} to ${TARGET_BRANCH} SYNC"

# Get all open merge requests and check if one exists for source branch
LISTMR=`curl -L --silent "${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${PERSONAL_ACCESS_TOKEN}"`;
COUNTBRANCHES=`echo ${LISTMR} | grep -o "\"source_branch\":\"${CI_COMMIT_REF_NAME}\"" | wc -l`;

echo "CI_SERVER_HOST: ${CI_SERVER_HOST}"
echo "CI_PROJECT_ID: ${CI_PROJECT_ID}"
echo "CI_COMMIT_REF_NAME: ${CI_COMMIT_REF_NAME}"
echo "TARGET_BRANCH: ${TARGET_BRANCH}"
echo "GITLAB_USER_ID: ${GITLAB_USER_ID}"
echo "PERSONAL_ACCESS_TOKEN: ${PERSONAL_ACCESS_TOKEN}"

# If no MR found, create a new one
if [ ${COUNTBRANCHES} -eq "0" ]; then
  curl -L --request POST "${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/merge_requests?id=${CI_PROJECT_ID}&source_branch=${CI_COMMIT_REF_NAME}&target_branch=${TARGET_BRANCH}&title=${TITLE}&remove_source_branch=true&assignee_id=${GITLAB_USER_ID}" --header "Content-Type:application/json" --header "PRIVATE-TOKEN:${PERSONAL_ACCESS_TOKEN}"
  echo "Opened a new merge request: ${TITLE} and assigned to ${GITLAB_USER_NAME}";
  exit;
fi

echo "No new merge request opened. Merge Request already exists";
