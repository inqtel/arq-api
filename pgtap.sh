#! /bin/bash

echo "running migrations"
for f in $(ls hasura/migrations/*/*.sql | sort -V); do
  echo "psql --quiet -h 'registry.gitlab.com-inqtel-postgres12-pgtap' -U '$POSTGRES_USER' -d '$POSTGRES_DB' -f $f"
  psql --quiet -h "registry.gitlab.com-inqtel-postgres12-pgtap" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -f $f
done;

echo "planting seeds"
for f in $(ls hasura/seeds/*.sql | sort -V); do
  echo "psql --quiet -h 'registry.gitlab.com-inqtel-postgres12-pgtap' -U '$POSTGRES_USER' -d '$POSTGRES_DB' -f $f"
  psql --quiet -h "registry.gitlab.com-inqtel-postgres12-pgtap" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -f $f
done;

echo "recompiling functions for coverage"
_config="
piggly:
  adapter: postgresql
  database: $POSTGRES_DB
  username: $POSTGRES_USER
  password: $POSTGRES_PASSWORD
  host: $DATABASE_HOST
"
mkdir piggly
echo "$_config" > piggly/config.yaml
cat piggly/config.yaml
piggly trace -d piggly/config.yaml -c "piggly/cache" -o "piggly/reports"

echo "running pgtap"
for f in $(ls postgres/pgtap/test*.sql | sort -V); do
  echo "pg_prove -h 'registry.gitlab.com-inqtel-postgres12-pgtap' -U '$POSTGRES_USER' -d '$POSTGRES_DB' $f"
  pg_prove -h "registry.gitlab.com-inqtel-postgres12-pgtap" -U "$POSTGRES_USER" -d "$POSTGRES_DB" $f 2>> piggly/trace.txt
done;

echo "writing out reports"
piggly report -o "piggly/reports" -c "piggly/cache" -f "piggly/trace.txt"

echo "uncompiling coverage functions"
piggly untrace -d piggly/config.yaml -c "piggly/cache"

echo "dumping reports"
echo "OK, view piggly/reports/index.html"
cut -c -600 piggly/reports/index.html